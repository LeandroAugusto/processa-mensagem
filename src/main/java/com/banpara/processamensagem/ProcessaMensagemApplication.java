package com.banpara.processamensagem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@EnableBatchProcessing
@SpringBootApplication
public class ProcessaMensagemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProcessaMensagemApplication.class, args);
	}

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldApplication.class, args);
	}

	@Bean
	public Step step() {
		return stepBuilderFactory.get("step1").tasklet(new Tasklet() {
			@Override
			public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				System.out.println("Hello, World!");
				return RepeatStatus.FINISHED;
			}
		}).build();
	}

	@Bean
	public Job job() {
		return this.jobBuilderFactory.get("nomeDoJob").start(step()).build();
	}
}
